package com.zs.algorithmpractice;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * @author: zhangshao
 * @time: 2019/5/15 10:34
 * @description:
 */
public class FifteenTo100SolutionTest {

    FifteenTo100Solution solution;

    @Before
    public void init(){
        solution = new FifteenTo100Solution();
    }

    @Test
    public void deleteDuplicatesTest(){
        FifteenTo100Solution.ListNode node = new FifteenTo100Solution.ListNode(1);
        FifteenTo100Solution.ListNode head = node;
        for(int i=0;i<4;i++){
            node.next = new FifteenTo100Solution.ListNode(1);
            node = node.next;
//            node.next.next = new FifteenTo100Solution.ListNode(i+1);
//            node = node.next.next;
        }
        printNode(head);
        solution.deleteDuplicates(head);
        printNode(head);
    }

    private void printNode(FifteenTo100Solution.ListNode node){
        while (node!=null){
            System.out.print(node.val+",");
            node = node.next;
        }
        System.out.println("");
    }

//    @Test
    public void mergeArrTest(){
        int[] a = new int[]{1,2,3,8,12,0,0,0,0,0};
        int[] b = new int[]{5,6,7,8,9};
        solution.merge(a,5,b,5);
    }

//    @Test
    public void mySqrtTest(){
        int x = 11;
        int result1 = solution.mySqrt(x);
        int result2 = (int)Math.sqrt(x);
        System.out.print(result1+"----"+result2);
        assertTrue(result1==result2);
    }

//    @Test
    public void climbStairsTest(){
//        System.out.print(solution.climbStairs(3));
        System.out.print(solution.removeDuplicates("1122334"));
    }

//    @Test
    public void addBinary(){
        String str = solution.addBinary("1111","1111");
        System.out.print(str);
    }

//    @Test
//    public void plusOne(){
//       int[] arr = new int[]{9,9,9,9};
//       arr = solution.plusOne(arr);
//       for(int i:arr){
//           System.out.print(i+",");
//       }
//    }

//    @Test
    public void lengthOfLastWord() {
        int len0 = solution.lengthOfLastWord(null);
        assertTrue(len0==0);

        int len1 = solution.lengthOfLastWord("Hello World");
        assertTrue(len1==5);

        int len2 = solution.lengthOfLastWord("He           ");
        assertTrue(len2==2);

        int len3 = solution.lengthOfLastWord("         ");
        assertTrue(len3==0);
    }

//    @Test
    public void lengthOfLastWord1() {
        int len0 = solution.lengthOfLastWord1(null);
        assertTrue(len0==0);

        int len1 = solution.lengthOfLastWord1("Hello World");
        assertTrue(len1==5);

        int len2 = solution.lengthOfLastWord1("He           ");
        assertTrue(len2==2);

        int len3 = solution.lengthOfLastWord1("         ");
        assertTrue(len3==0);
    }
}