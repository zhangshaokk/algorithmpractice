package com.zs.algorithmpractice;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;


/**
 * @author: zhangshao
 * @time: 2019/5/14 12:08
 * @description:
 */
public class ZeroTo50SolutionTest {
    ZeroTo50Solution solution;

    @Before
    public void init() {
        solution = new ZeroTo50Solution();
    }

    @Test
    public void SeventeenthTest(){
        List<String> list = solution.letterCombinations("");
        System.out.println(Arrays.toString(list.toArray()));
    }

//    @Test
    public void threeSumClosestTest(){
        int[] nums = new int[]{-3,-2,-5,3,-4};
        int target = -1;
        int result = solution.threeSumClosest(nums,target);
        System.out.print("result:"+result);
    }

//    @Test
    public void intToRomanTest(){
        String result = solution.intToRoman(1994);
        System.out.print("result:"+result);
    }

//    @Test
    public void maxAreaTest(){
        int result = solution.maxArea(new int[]{1,8,6,2,5,4,8,3,7});
        System.out.print("result:"+result);
    }

    //    @Test
    public void solutionTest() {

        int result = solution.strStr3("baadbssefbabba", "bba");
        System.out.print("result:" + result);

        solution.lengthOfLongestSubstring("");
        solution.generate(8);

        System.out.println("result:" + solution.isPalindrome("race a car"));
        System.out.print("result:" + solution.countAndSay(6));
        int[] nums = new int[]{-10, -2, -3, -4, -5, 1, 0, -9};
        System.out.print("result:" + solution.maxSubArray(nums));

        String[] strs = new String[]{"aa", "a"};
        System.out.print("result:" + solution.longestCommonPrefix(strs));
    }

//    @Test
    public void divideTest(){
        int a = 100;
        int b = 3;
        int i=solution.divide(a,b);
        int result = a/b;
        System.out.print(i);
        assert (result==i);
//        System.out.print(solution.lastStoneWeight(new int[]{2,7,4,1,8,1}));
    }

//    @Test
    public void longestPalindromeTest(){
        String result = solution.longestPalindrome("babad");
        System.out.print("result:"+result);
    }


//    @Test
    public void myAtoiTest(){
       int resutl =  solution.myAtoi("+  0000000000012345678");
//        int resutl =  2147483647 *5;
       System.out.print(resutl);
    }

//    @Test
    public void isValidSudokuTest() {
        char[][] chars = new char[][]{{'5', '3', '.', '.', '7', '.', '.', '.', '.'},
                {'6', '.', '.', '1', '9', '5', '.', '.', '.'},
                {'.', '9', '8', '.', '.', '.', '.', '6', '.'},
                {'8', '.', '.', '.', '6', '.', '.', '.', '3'},
                {'4', '.', '.', '8', '.', '3', '.', '.', '1'},
                {'7', '.', '.', '.', '2', '.', '.', '.', '6'},
                {'.', '6', '.', '.', '.', '.', '2', '8', '.'},
                {'.', '.', '.', '4', '1', '9', '.', '.', '5'},
                {'.', '.', '.', '.', '8', '.', '.', '7', '9'}};
        boolean result = solution.isValidSudoku(chars);
        assert (result);

        char[][] chars1 = new char[][]{{'8', '3', '.', '.', '7', '.', '.', '.', '.'},
                {'6', '.', '.', '1', '9', '5', '.', '.', '.'},
                {'.', '9', '8', '.', '.', '.', '.', '6', '.'},
                {'8', '.', '.', '.', '6', '.', '.', '.', '3'},
                {'4', '.', '.', '8', '.', '3', '.', '.', '1'},
                {'7', '.', '.', '.', '2', '.', '.', '.', '6'},
                {'.', '6', '.', '.', '.', '.', '2', '8', '.'},
                {'.', '.', '.', '4', '1', '9', '.', '.', '5'},
                {'.', '.', '.', '.', '8', '.', '.', '7', '9'}};
        boolean r1 = solution.isValidSudoku(chars1);
        assert (!r1);


        char[][] c2 = new char[][]{{'.', '.', '.', '.', '5', '.', '.', '1', '.'},
                                    {'.', '4', '.', '3', '.', '.', '.', '.', '.'},
                                    {'.', '.', '.', '.', '.', '3', '.', '.', '1'},
                                    {'8', '.', '.', '.', '.', '.', '.', '2', '.'},
                                    {'.', '.', '2', '.', '7', '.', '.', '.', '.'},
                                    {'.', '1', '5', '.', '.', '.', '.', '.', '.'},
                                    {'.', '.', '.', '.', '.', '2', '.', '.', '.'},
                                    {'.', '2', '.', '9', '.', '.', '.', '.', '.'},
                                    {'.', '.', '4', '.', '.', '.', '.', '.', '.'}};
        boolean r2 = solution.isValidSudoku(c2);
        assert (!r2);
    }

//    @Test
    public void findMedianSortedArraysTest(){
        int[] nums1 = new int[]{1};
        int[] nums2 = new int[]{2};
        double resutl = solution.findMedianSortedArrays(nums1,nums2);
        System.out.print("result:"+resutl);
    }
}