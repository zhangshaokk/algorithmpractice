package com.zs.algorithmpractice;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author: zhangshao
 * @time: 2019/5/31 10:39
 * @description:
 */
public class OneHsTo150SolutionTest {

    OneHsTo150Solution solution;

    @Before
    public void init(){
        solution = new OneHsTo150Solution();
    }

    @Test
    public void minDepthTest(){
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);

        System.out.print(solution.minDepth(root));
    }
}