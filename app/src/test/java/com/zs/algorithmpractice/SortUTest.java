package com.zs.algorithmpractice;

import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;

/**
 * @author: zhangshao
 * @time: 2019/5/14 12:12
 * @description:
 */
public class SortUTest {
//    int[] arr1 = new int[]{1,3,4,6,7,8,9,12,14,15};
//    int[] arr2 = new int[]{3,1,2,56,75,4,5,90,34,13};
//    int[] arr3 = new int[]{1,1,1,33,44,3,54,65,67,65,7,45};

    @Test
    public void bubbleSort() {
        int[] tempArr = createArray();
        SortU.bubbleSort(tempArr);
        assertTrue (SortU.checkResult(tempArr));

        tempArr = createArray();
        SortU.selectionSort(tempArr);
        assertTrue (SortU.checkResult(tempArr));

        tempArr = createArray();
        SortU.insertSort(tempArr);
        assertTrue (SortU.checkResult(tempArr));

        tempArr = createArray();
        SortU.quickSortR(tempArr);
        assertTrue (SortU.checkResult(tempArr));

    }

    private int[] createArray(){
        int[] arr = new int[12];
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for(int i=0;i<arr.length;i++){
            arr[i] = new Random().nextInt(12);
            sb.append(arr[i]);
            if(i!=arr.length-1){
                sb.append(",");
            }
        }
        sb.append("]");
        System.out.println("before:"+sb.toString());
        return arr;
    }
}