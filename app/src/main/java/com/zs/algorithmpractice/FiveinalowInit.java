package com.zs.algorithmpractice;

import java.util.Arrays;
import java.util.Random;

/**
 * @author: zs
 * @time: 2020/9/9 15:42
 * @description: 7*7 棋盘 ，黑白棋初始化
 */
class FiveinalowInit {

    private int[][] chessboard = new int[7][7];


    private void initChessboard() {
        Random random = new Random();
        for (int x = 0; x < 7; x++) {
            for (int i = 0; i < 7; i++) {
                int r = random.nextInt(100) + 1;
                chessboard[x][i] = r <= 50 ? 0 : 1;
            }
        }
    }

    int times = 1;

    private void landTrim() {
        for (int i = 0; i < 7; i++) {
            if (chessboard[i][3] == chessboard[i][2]) {
                chessboard[i][3] = (chessboard[i][3] + 1) % 2;
//                System.out.println("第"+i+"行横向，总次数:"+times);
                times++;
            }
        }
    }

    private void poritraTrim() {
        for (int i = 0; i < 7; i++) {
            if (chessboard[3][i] == chessboard[2][i]) {
                chessboard[3][i] = (chessboard[3][i] + 1) % 2;
//                System.out.println("第"+i+"列纵向，总次数:"+times);
                times++;
            }
        }
    }

    private void leftSlanTrim() {
        if (chessboard[5][3] == chessboard[4][2]) {
            chessboard[5][3] = (chessboard[5][3] + 1) % 2;
//            System.out.println("第0列左斜，总次数:"+times);
            times++;
        }
        if (chessboard[4][3] == chessboard[3][2]) {
            chessboard[4][3] = (chessboard[4][3] + 1) % 2;
//            System.out.println("第1列左斜，总次数:"+times);
            times++;
        }
        if (chessboard[3][3] == chessboard[2][2]) {
            chessboard[2][2] = (chessboard[2][2] + 1) % 2;
//            System.out.println("第2列左斜，总次数:"+times);
            times++;
        }
        if (chessboard[3][4] == chessboard[2][3]) {
            chessboard[3][4] = (chessboard[3][4] + 1) % 2;
//            System.out.println("第3列左斜，总次数:"+times);
            times++;
        }
        if (chessboard[3][5] == chessboard[2][4]) {
            chessboard[3][5] = (chessboard[3][5] + 1) % 2;
//            System.out.println("第4列左斜，总次数:"+times);
            times++;
        }
    }

    private void rightSlanTrim() {
        if (chessboard[3][1] == chessboard[2][2]) {
            chessboard[3][1] = (chessboard[3][1] + 1) % 2;
//            System.out.println("第0列右斜，总次数:"+times);
            times++;
        }
        if (chessboard[3][2] == chessboard[2][3]) {
            chessboard[3][2] = (chessboard[3][2] + 1) % 2;
//            System.out.println("第1列右斜，总次数:"+times);
            times++;
        }
        if (chessboard[3][3] == chessboard[2][4]) {
            chessboard[2][4] = (chessboard[2][4] + 1) % 2;
//            System.out.println("第2列右斜，总次数:"+times);
            times++;
        }
        if (chessboard[4][3] == chessboard[3][4]) {
            chessboard[3][4] = (chessboard[3][4] + 1) % 2;
//            System.out.println("第3列右斜，总次数:"+times);
            times++;
        }
        if (chessboard[5][3] == chessboard[4][4]) {
            chessboard[5][3] = (chessboard[5][3] + 1) % 2;
//            System.out.println("第4列右斜，总次数:"+times);
            times++;
        }
    }

    public void printChess() {
        long time0 = System.currentTimeMillis();
        int n = 1;
        int useful = 0;
        while (useful <= 100) {
            initChessboard();
            landTrim();
            poritraTrim();
            leftSlanTrim();
            rightSlanTrim();
            long t1 = System.currentTimeMillis();
            System.out.println("第" + n + "次调整耗时:" + (t1 - time0) + "ms");
            boolean r = allCheck();
            System.out.println("第" + n + "次校验耗时:" + (System.currentTimeMillis() - t1) + "ms");
            if(r) {useful++;}
            n++;
        }
        print();
//        System.out.println("直接生成有效随机数组次数为:"+f);
        System.out.println("共调整"+n+"次,总耗时:" + (System.currentTimeMillis() - time0) + "msg");
        System.out.println("生成有效序列次数:"+useful);
    }

    private boolean allCheck() {
        for (int i = 0; i < 7; i++) {
            // 横向检查
            int sum = 0;
            if (chessboard[i][3] != chessboard[i][2] && chessboard[i][3] != chessboard[i][4]) {
                continue;
            }
            for (int n = 0; n < 6; n++) {
                if (chessboard[i][n] == chessboard[i][n + 1]) {
                    sum += 1;
                } else {
                    sum = 0;
                }
                if (sum >= 4) {
                    System.out.println("第" + i + "行连续");
                    return false;
                }
            }
        }

        //纵向检查
        for (int i = 0; i < 7; i++) {
            int sum = 0;
            if (chessboard[3][i] != chessboard[2][i] && chessboard[3][i] != chessboard[4][i]) {
                continue;
            }
            for (int n = 0; n < 6; n++) {
                if (chessboard[n][i] == chessboard[n + 1][i]) {
                    sum += 1;
                } else {
                    sum = 0;
                }
                if (sum >= 4) {
                    System.out.println("第" + i + "列连续");
                    return false;
                }
            }
        }

        int lsum0 = 0, lsum1 = 0, lsum2 = 0, lsum3 = 0, lsum4 = 0;
        for (int i = 0; i < 7; i++) {
            if (i < 5) {
                lsum0 += chessboard[2 + i][i];
                lsum4 += chessboard[i][i + 2];
            }
            if (i < 6) {
                lsum1 += chessboard[1 + i][i];
                lsum3 += chessboard[i][i + 1];
            }
            lsum2 += chessboard[i][i];
            if (lsum2 >= 5 || lsum0 >= 5 || lsum1 >= 5 || lsum3 >= 5 || lsum4 >= 5) {
                System.out.println("存在左斜连续");
                return false;
            }
        }

        lsum0 = 0; lsum1 = 0; lsum2 = 0; lsum3 = 0; lsum4 = 0;
        for (int i = 0; i < 7; i++) {
            if (i < 5) {
                lsum0 += chessboard[i][4 - i];
                lsum3 += chessboard[i + 2][6 - i];
            }
            if (i < 6) {
                lsum1 += chessboard[i][5 - i];
                lsum4 += chessboard[i + 1][6 - i];
            }
            lsum2 += chessboard[i][6 - i];
            if (lsum2 >= 5 || lsum0 >= 5 || lsum1 >= 5 || lsum3 >= 5 || lsum4 >= 5) {
                System.out.println("存在右斜连续");
                return false;
            }
        }
        return true;
    }

    private void print() {
        for (int[] a : chessboard) {
            System.out.println(Arrays.toString(a));
        }
    }
}
