package com.zs.algorithmpractice.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author: zhangshao
 * @time: 2019/7/22 10:29
 * @description:
 */
public class AndroidDimensUtils {

    public static void gen() {
        //以此文件夹下的dimens.xml文件内容为初始值参照
        File file = new File("/Users/zhj/Downloads/le/app/src/main/res/values-xxxhdpi/dimens.xml");

        BufferedReader reader = null;
        StringBuilder hdpi = new StringBuilder();
        StringBuilder xhdpi = new StringBuilder();
        StringBuilder xxhdpi = new StringBuilder();
        try {
            System.out.println("生成不同分辨率：");
            reader = new BufferedReader(new FileReader(file));
            String tempString;


            // 一次读入一行，直到读入null为文件结束
            while ((tempString = reader.readLine()) != null) {
                if (tempString.contains("</dimen>")) {
                    //tempString = tempString.replaceAll(" ", "");
                    String start = tempString.substring(0, tempString.indexOf(">") + 1);
                    String end = tempString.substring(tempString.lastIndexOf("<") - 2);
                    //截取<dimen></dimen>标签内的内容，从>右括号开始，到左括号减2，取得配置的数字
                    float num = Float.parseFloat
                            (tempString.substring(tempString.indexOf(">") + 1,
                                    tempString.indexOf("</dimen>") - 2));

                    //根据不同的尺寸，计算新的值，拼接新的字符串，并且结尾处换行。
                    float hnum = num * 0.25f;
                    float xhnum = num * 0.5f;
                    float xxhnum = num * 0.75f;
                    hdpi.append(start).append(hnum).append(end).append("\r\n");
                    xhdpi.append(start).append(xhnum).append(end).append("\r\n");
                    xxhdpi.append(start).append(xxhnum).append(end).append("\r\n");
                } else {
                    hdpi.append(tempString).append("");
                    xhdpi.append(tempString).append("");
                    xxhdpi.append(tempString).append("");
                }
            }
            reader.close();
            String hfile = "/Users/zhj/Downloads/le/app/src/main/res/values-hdpi/dimens.xml";
            String xhfile = "/Users/zhj/Downloads/le/app/src/main/res/values-xhdpi/dimens.xml";
            String xxhfile = "/Users/zhj/Downloads/le/app/src/main/res/values-xxhdpi/dimens.xml";
            //将新的内容，写入到指定的文件中去
            writeFile(hfile, hdpi.toString());
            writeFile(xhfile, xhdpi.toString());
            writeFile(xxhfile, xxhdpi.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

            }

        }

    }


    /**
     * 写入方法
     */

    private static void writeFile(String file, String text) {

        PrintWriter out = null;

        try {
            out = new PrintWriter(new BufferedWriter(new FileWriter(file)));
            out.println(text);
        } catch (IOException e) {
            e.printStackTrace();

        }


        out.close();

    }

}
