package com.zs.algorithmpractice;

import java.util.ArrayList;
import java.util.List;

public class SortU {

    public static void bubbleSort(int[] arr) {
        if (!checkArray(arr)) return;

        boolean flag = false;
        int temp;
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = arr.length - 1; j > i; j--) {
                if (arr[j] < arr[j - 1]) {
                    swap(arr, j, j - 1);
                    flag = true;
                }
            }
            if (!flag) break;
        }
    }

    private static boolean checkArray(int[] arr) {
        return arr != null && arr.length > 0;
    }

    public static boolean checkResult(int[] arr) {
        if (arr.length == 1) {
            System.out.println("[" + arr[0] + "]");
            return true;
        }
        boolean result = true;
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < arr.length - 1; i++) {
            sb.append(arr[i]).append(",");
            if (i == arr.length - 2) sb.append(arr[i + 1]);
            if (arr[i] > arr[i + 1]) {
                result = false;
            }
        }
        sb.append("]");
        System.out.println("result:" + sb.toString());
        return result;
    }

    public static void selectionSort(int[] arr) {
        if (!checkArray(arr)) return;

        int minIndex;
        int temp;
        for (int i = 0; i < arr.length - 1; i++) {
            minIndex = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < arr[minIndex]) {
                    minIndex = j;
                }
            }
            if (i != minIndex) {
                swap(arr, i, minIndex);
            }
        }
    }

    public static void insertSort(int[] arr) {
        if (!checkArray(arr)) return;
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i + 1; j > 0; j--) {
                if (arr[j] < arr[j - 1]) {
                    swap(arr, j, j - 1);
                } else {
                    break;
                }
            }
        }
    }


    //递归快排
    public static void quickSortR(int[] arr) {
        if (!checkArray(arr)) return;

        quicksort(arr, 0, arr.length - 1);
    }

    private static void quickSortRInternal(int[] arr, int left, int right) {
        if (left >= right) return;

        int key = arr[left];//基准数
        int l = left;
        int r = right;

        while (l <= r) {
            while (l < r && arr[r] >= key) r--;

            while (l < r && arr[l] <= key) l++;

            if (l < r) {
                swap(arr, l, r);
            }
        }
        //此处l=r,基数归位
        arr[l] = key;
        quickSortRInternal(arr, left, l - 1);
        quickSortRInternal(arr, r + 1, right);
    }

    private static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    public static void quicksort(int[] a, int left, int right) {
        int i, j, t, temp;
        if (left > right)
            return;

        temp = a[left]; //temp中存的就是基准数
        i = left;
        j = right;
        while (i != j) {
            //顺序很重要，要先从右边开始找
            while (a[j] >= temp && i < j)
                j--;
            //再找右边的
            while (a[i] <= temp && i < j)
                i++;
            //交换两个数在数组中的位置
            if (i < j) {
                t = a[i];
                a[i] = a[j];
                a[j] = t;
            }
        }
        //最终将基准数归位
        a[left] = a[i];
        a[i] = temp;

        quicksort(a, left, i - 1);//继续处理左边的，这里是一个递归的过程
        quicksort(a, i + 1, right);//继续处理右边的 ，这里是一个递归的过程
    }

}
