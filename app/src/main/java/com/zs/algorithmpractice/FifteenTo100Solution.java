package com.zs.algorithmpractice;

import java.util.Arrays;

/**
 * @author: zhangshao
 * @time: 2019/5/15 10:31
 * @description: 51~100
 */
public class FifteenTo100Solution {

    /**
     * 58. 最后一个单词的长度
     * 方法一:分割字符串，然后取最末尾的长度
     */
    public int lengthOfLastWord(String s) {
        if (s == null) return 0;
        String[] strs = s.split(" ");
        return strs.length == 0 ? 0 : strs[strs.length - 1].length();
    }

    /**
     * 58. 最后一个单词的长度
     * 方法二:遍历过程中记录单词长度
     */
    public int lengthOfLastWord1(String s) {
        int result = 0;
        if (s == null) return result;
        int len = s.length();
        if (len == 0) return result;
        char spcae = " ".charAt(0);
        int temp = 0;
        for (int i = 0; i < len; i++) {
            if (s.charAt(i) == spcae) {
                if (result != 0) {
                    temp = result;
                }
                result = 0;
            } else {
                result++;
            }

        }
        return result == 0 ? temp : result;
    }

    /**
     * 66.加一
     * 给定一个由整数组成的非空数组所表示的非负整数，在该数的基础上加一。
     * 最高位数字存放在数组的首位， 数组中每个元素只存储一个数字。
     * 你可以假设除了整数 0 之外，这个整数不会以零开头。
     */
    public int[] plusOne(int[] digits) {
        boolean flag = false;
        for (int i = digits.length - 1; i >= 0; i--) {
            digits[i] += 1;
            flag = (digits[i] >= 10);
            if (flag) {
                digits[i] = digits[i] % 10;
                continue;
            }
            break;
        }
        if (flag) {
            int[] newArr = new int[digits.length + 1];
            newArr[0] = 1;
            for (int j = 0; j < digits.length; j++) {
                newArr[j + 1] = digits[j];
            }
            return newArr;
        }
        return digits;
    }

    /**
     * 67. 二进制求和
     */
    public String addBinary(String a, String b) {
        int lenA = a.length();
        int lenB = b.length();
        String longStr, shortStr;
        if (lenA > lenB) {
            longStr = a;
            shortStr = b;
        } else {
            longStr = b;
            shortStr = a;
        }

        char[] carr = new char[longStr.length()];
        boolean flag = false;
        for (int i = carr.length - 1; i >= 0; i--) {
            int shortIndex = i - carr.length + shortStr.length();
            int result;
            if (shortIndex >= 0) {
                int carry = flag ? 1 : 0;
                result = (longStr.charAt(i) - '0') + (shortStr.charAt(shortIndex) - '0') + carry;
            } else {
                int carry = flag ? 1 : 0;
                result = longStr.charAt(i) - '0' + carry;
            }
            flag = (result >= 2);
            if (flag) {
                result %= 2;
            }
            carr[i] = (char) (result + 48);
        }
        if (flag) {
            String res = new String(carr);
            return "1" + res;
        }
        return new String(carr);
    }


    /**
     * 70.爬楼梯
     * 每次只翻动1或2阶
     * 实际上可以看成，当前阶级的到达方式，实际上是往前回退1阶的方法数和回退2阶的方法数之和
     * 既有 S[n] = S[n-1] + S[n-2]
     */
    public int climbStairs1(int n) {
        int[] S = new int[n];
        for (int i = 0; i < n; i++) {
            if (i < 3) {
                S[i] = i + 1;
            } else {
                S[i] = S[i - 1] + S[i - 2];
            }
        }
        return S[n - 1];
    }


    /**
     * 70.爬楼梯
     * 每次只翻动1或2阶
     * 实际上可以看成，当前阶级的到达方式，实际上是往前回退1阶的方法数和回退2阶的方法数之和
     * 既有 S[n] = S[n-1] + S[n-2]
     * <p>
     * 方法2：climbStairs1中需要额外建立一个新数组，导致内存上有一定开销
     * 实际上，此
     */
    public int climbStairs(int n) {
        if (n == 1) return 1;
        int sum = 2;
        int p1 = 1;
        int temp;
        for (int i = 3; i <= n; i++) {
            temp = sum;
            sum += p1;
            p1 = temp;
        }
        return sum;
    }


    /**
     * 竞赛题：5064. 删除字符串中的所有相邻重复项
     *
     * @param S
     * @return
     */
    public String removeDuplicates(String S) {
        int len = S.length();
        char[] cache = new char[len];
        int cIndex = 0;
        char temp;
        for (int i = 0; i < len; i++) {
            temp = S.charAt(i);
            if (cache[cIndex] == '\0') {
                cache[cIndex] = temp;
                continue;
            }

            if (cache[cIndex] == temp) {
                cache[cIndex--] = '\0';
                if (cIndex < 0) cIndex = 0;
            } else {
                cache[++cIndex] = temp;
            }
        }
        if (cache[0] == '\0') return "";
        return new String(cache, 0, cIndex + 1);
    }


    /**
     * 69.x的平方根
     */
    public int mySqrt(int x) {
        if (x == 0) return 0;
        if (x < 4) return 1;
        int result = x >> 1;
        int left = 0, right = x;
        while (left < right) {
            int temp = result * result;
            if (temp < 0 || result > 46340) {
                right = result;
                result >>= 1;
                continue;
            }
            if (temp == x) {
                return result;
            } else if (temp > x) {
                right = result;
                result = (left + right) / 2;
            } else {
                left = result;
                result = (left + right) / 2;
                if (left == result) break;
            }
        }
        return result;
    }

    /**
     * 88.合并两个有序数组
     * 说明:
     * 初始化 nums1 和 nums2 的元素数量分别为 m 和 n。
     * 你可以假设 nums1 有足够的空间（空间大小大于或等于 m + n）来保存 nums2 中的元素
     */
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        while (m + n > 0) {
            if (m == 0) {
                nums1[n - 1] = nums2[n - 1];
                n--;
            } else if (n == 0) {
                m--;
            } else if (nums1[m - 1] > nums2[n - 1]) {
                nums1[m + n - 1] = nums1[m - 1];
                m--;
            } else {
                nums1[m + n - 1] = nums2[n - 1];
                n--;
            }
        }
        System.out.println(Arrays.toString(nums1));
    }


    /**
     * 83.删除链表中的重复元素
     * 针对排序列表的方案 二
     */
    public ListNode deleteDuplicates(ListNode head) {
        if(head == null || head.next == null)
            return head;
        ListNode prev = head;
        ListNode p = head.next;
        while(p != null){
            if(p.val == prev.val){
                prev.next = p.next;
                p = p.next;
                //no change prev
            }else{
                prev = p;
                p = p.next;
            }
        }
        return head;
    }

    /**
     * 83.删除链表中的重复元素
     * 此方案可针对未排序的列表，针对题目中给点的排序列表，可以有另外的方案
     * @param head
     * @return
     */
    public ListNode deleteDuplicates1(ListNode head) {
        ListNode temp;
        ListNode cL = head;
        int c;
        while(cL!=null){
            c = cL.val;
            temp = cL;
            while(temp.next !=null){
                if(c == temp.next.val){
                    temp.next = temp.next.next;
                    continue;
                }
                temp = temp.next;
            }
            cL = cL.next;
        }
        return head;
    }


    public static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }


    /**
     * 100.相同二叉树
     * @param p
     * @param q
     * @return
     */
    public boolean isSameTree(TreeNode p, TreeNode q) {
        if(p==null || q==null) return p==q;

        return p.val==q.val && isSameTree(p.left,q.left) && isSameTree(p.right,q.right);
    }

}
