package com.zs.algorithmpractice.lentcode;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author: zs
 * @time: 10/14/21 14:02
 * @description:领扣-合并区间
 */
public class Merge {

    public class Interval {
        int start, end;

        Interval(int start, int end) {
            this.start = start;
            this.end = end;
        }
    }

    public class Solution {
        /**
         * @param intervals: interval list.
         * @return: A new interval list.
         */
        public List<Interval> merge(List<Interval> intervals) {
            // 排序
            Collections.sort(intervals, new Comparator<Interval>() {
                @Override
                public int compare(Interval interval, Interval t1) {
                    if(interval.start==t1.start)
                        return interval.end-t1.end;
                    return interval.start-t1.start;
                }
            });

            int last = -1;
            int index = 0;
            for (Interval item : intervals) {
                if (last == -1 || last < item.start) {
                    intervals.get(index).start = item.start;
                    intervals.get(index).end = item.end;
                    last = item.end;
                    index++;
                } else {
                    last = Math.max(last, item.end);
                    intervals.get(index - 1).end = last;
                }
            }

            return intervals.subList(0, index);
        }
    }
}
