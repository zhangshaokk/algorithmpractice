package com.zs.algorithmpractice;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * @author: zhangshao
 * @time: 2019/5/31 10:38
 * @description:
 */
public class OneHsTo150Solution {

    /**
     * 101.对称二叉树
     * @param root
     * @return
     */
    public boolean isSymmetric(TreeNode root) {
        if(root==null) return true;
        Queue<TreeNode> q = new LinkedList<>();
        q.add(root.left);
        q.add(root.right);
        TreeNode left,right;
        while(!q.isEmpty()){
            left = q.poll();
            right = q.poll();
            if(left==null || right==null){
                if(left==right) continue;
                else return false;
            }
            if(left.val!=right.val) return false;
            q.add(left.left);
            q.add(right.right);
            q.add(left.right);
            q.add(right.left);
        }
        return true;
    }


    /**
     * 118.杨辉三角
     */
    public List<List<Integer>> generate(int numRows) {
        ArrayList<List<Integer>> lists = new ArrayList<List<Integer>>();
        for (int i = 0; i < numRows; i++) {
            ArrayList<Integer> list = new ArrayList<Integer>();
            lists.add(list);
            for (int j = 0; j <= i; j++) {
                if (i == 0 || j == 0 || j == i) {
                    list.add(1);
                } else {
                    ArrayList<Integer> temp = (ArrayList<Integer>) lists.get(i - 1);
                    Integer value = temp.get(j) + temp.get(j-1);
                    list.add(value);
                }
            }
        }
        return lists;
    }

    /**
     * 119.杨辉三角(二)
     */
    public List<Integer> getRow(int rowIndex) {
        List<Integer> res = new ArrayList<>(rowIndex + 1);
        long cur = 1;
        for (int i = 0; i <= rowIndex; i++) {
            res.add((int) cur);
            cur = cur * (rowIndex-i)/(i+1);
        }
        return res;
    }


    /**
     * 125.验证回文串
     */
    public boolean isPalindrome(String s) {
        if (s==null) return false;
        if(s.length()==0) return true;
        int sIndex = 0;
        int endIndex = s.length()-1;
        while(sIndex<endIndex){
            int sChr = s.charAt(sIndex);
            if(!isDigitOrChar(sChr)){
                sIndex++;
                continue;
            }
            int eChr = s.charAt(endIndex);
            if(!isDigitOrChar(eChr)){
                endIndex--;
                continue;
            }

            sChr = lowcase(sChr);
            eChr = lowcase(eChr);
            if(sChr!=eChr){
                return false;
            }
            sIndex++;
            endIndex--;
        }
        return true;
    }

    // 125
    private int lowcase(int v){
        if(v>=97 && v <=122){
            return v-32;
        }
        return v;
    }

    //125
    private boolean isDigitOrChar(int v){
        return (v>=48 && v<=57) || (v>=65 && v <= 90) || (v>=97 && v <=122);
    }


    /**
     * 104.二叉树的最大深度
     */
    public int maxDepth(TreeNode root) {
        if(root == null) return 0;
        int deep = getTreeDeep(root);
        return deep;
    }

    //104
    private int getTreeDeep(TreeNode node){
        if(node == null) return 0;

        int leftd = getTreeDeep(node.left);
        int rightd = getTreeDeep(node.right);

        return leftd > rightd? leftd+1:rightd+1;
    }


    /**
     * 111.二叉树的最小深度
     */
    public int minDepth(TreeNode root) {
        if(root==null) return 0;
        int left = minDepth(root.left);
        int right = minDepth(root.right);
        return (left==0||right==0)?left+right+1:Math.min(left,right)+1;
    }
}
