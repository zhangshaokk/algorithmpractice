package com.zs.algorithmpractice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author: zhangshao
 * @time: 2019/5/14 09:57
 * @description: 1~50题解答
 */
public class ZeroTo50Solution {


    /**
     * 4.寻找两个有序数组的中位置数
     * 给定两个大小为 m 和 n 的有序数组 nums1 和 nums2。
     * 请你找出这两个有序数组的中位数，并且要求算法的时间复杂度为 O(log(m + n)
     * 此方法的时间复杂度为O((m+n)/2)  时间复杂度不符合
     */
    public double findMedianSortedArrays1(int[] nums1, int[] nums2) {
        int m, n;
        if (nums1 == null) m = 0;
        else m = nums1.length;

        if (nums2 == null) n = 0;
        else n = nums2.length;

        int mid = (m + n) / 2 + 1;
        int[] cache = new int[mid];
        for (int i = 0, j = 0, k = 0; i < mid; i++) {
            if (j >= m && k < n) {
                cache[i] = nums2[k++];
            } else if (k >= n && j < m) {
                cache[i] = nums1[j++];
            } else if (nums1[j] <= nums2[k]) {
                cache[i] = nums1[j++];
            } else {
                cache[i] = nums2[k++];
            }
        }

        if ((m + n) % 2 == 0) {
            double d = (cache[mid - 1] + cache[mid - 2]) / 2.0;
            return d;
        } else {
            return cache[mid - 1];
        }
    }


    /**
     * 4.寻找两个有序数组的中位置数
     * 给定两个大小为 m 和 n 的有序数组 nums1 和 nums2。
     * 请你找出这两个有序数组的中位数，并且要求算法的时间复杂度为 O(log(m + n)
     */
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int m = nums1.length, n = nums2.length;
        int l = (m + n + 1) / 2;
        int r = (m + n + 2) / 2;
        return (getkth(nums1, 0, nums2, 0, l) + getkth(nums1, 0, nums2, 0, r)) / 2.0;
    }

    public double getkth(int[] A, int aStart, int[] B, int bStart, int k) {
        if (aStart > A.length - 1) return B[bStart + k - 1];
        if (bStart > B.length - 1) return A[aStart + k - 1];
        if (k == 1) return Math.min(A[aStart], B[bStart]);

        int aMid = Integer.MAX_VALUE, bMid = Integer.MAX_VALUE;
        if (aStart + k / 2 - 1 < A.length) aMid = A[aStart + k / 2 - 1];
        if (bStart + k / 2 - 1 < B.length) bMid = B[bStart + k / 2 - 1];

        if (aMid < bMid)
            return getkth(A, aStart + k / 2, B, bStart, k - k / 2);// Check: aRight + bLeft
        else
            return getkth(A, aStart, B, bStart + k / 2, k - k / 2);// Check: bRight + aLeft
    }

    /**
     * 28.实现strStr()
     * 方案一：暴力比对，耗时太大(O(m*n))
     *
     * @param haystack
     * @param needle
     * @return
     */
    public int strStr1(String haystack, String needle) {
        if (haystack == null || needle == null) return -1;
        if ("".equals(needle)) return 0;
        if ("".equals(haystack)) return -1;
        char[] hayarr = haystack.toCharArray();
        char[] needarr = needle.toCharArray();
        //耗时太多，重复比较次数太多
        for (int i = 0; i < hayarr.length; i++) {
            for (int j = 0; j < needarr.length && (j + i) < hayarr.length; j++) {
                if (hayarr[j + i] != needarr[j]) {
                    break;
                }

                if (j == needarr.length - 1) {
                    return i;
                }
            }
        }
        return -1;
    }

    /**
     * 28.实现strStr()
     * 方案二: 优化版
     *
     * @param haystack
     * @param needle
     * @return
     */
    public int strStr2(String haystack, String needle) {
        if (haystack == null || needle == null) return -1;
        if ("".equals(needle)) return 0;
        if ("".equals(haystack)) return -1;
        char[] hayarr = haystack.toCharArray();
        char[] needarr = needle.toCharArray();
        int hayStart = 0;
        int needStart = 0;
        while (hayStart < hayarr.length && needStart < needarr.length) {
            if (hayarr[hayStart] == needarr[needStart]) {
                hayStart++;
                needStart++;
            } else {
                hayStart = hayStart - needStart + 1;
                needStart = 0;
            }
        }
        if (needStart == needarr.length) {
            return hayStart - needStart;
        }
        return -1;
    }


    /**
     * 28.实现strStr()
     * 方案二: KMP算法
     *
     * @param haystack
     * @param needle
     * @return
     */
    public int strStr3(String haystack, String needle) {
        if (haystack == null || needle == null) return -1;
        if ("".equals(needle)) return 0;
        if ("".equals(haystack)) return -1;
        char[] hayarr = haystack.toCharArray();
        char[] needarr = needle.toCharArray();
        int hayStart = 0;
        int needStart = 0;
        int[] next = getNext(needle);
        while (hayStart < hayarr.length && needStart < needarr.length) {
            if (needStart == -1 || hayarr[hayStart] == needarr[needStart]) {
                hayStart++;
                needStart++;
            } else {
                needStart = next[needStart];
            }
        }
        if (needStart == needarr.length) {
            return hayStart - needStart;
        }
        return -1;
    }

    private int[] getNext(String ps) {
        char[] p = ps.toCharArray();
        int[] next = new int[p.length];
        next[0] = -1;
        int j = 0;
        int k = -1;
        while (j < p.length - 1) {
            if (k == -1 || p[j] == p[k]) {
                if (p[++j] == p[++k]) { // 当两个字符相等时要跳过
                    next[j] = next[k];
                } else {
                    next[j] = k;
                }
            } else {
                k = next[k];
            }
        }
        return next;
    }


    /**
     * 查找数组中出现次数超过一半的元素
     *
     * @param a
     * @param n 数组长度
     * @return
     */
    public int half_number(int a[], int n) {
        if (a == null || n <= 0)
            return -1;

        int i, candidate = -1;
        int times = 0;
        for (i = 0; i < n; i++) {
            if (times == 0) {
                candidate = a[i];
                times = 1;
            } else if (a[i] == candidate)
                ++times;
            else
                --times;
        }
        return candidate;
    }

    /**
     * leetcode题3：无重复字符的最长子串
     * 给定一个字符串，找出不含重复字符的最长子串的长度
     */
    public int lengthOfLongestSubstring(String s) {
        char[] arr = s.toCharArray();
        if (arr.length <= 0) return 0;
        int subSIndex = 0, subEIndex = 0;
        int lSIndex = 0, lEIndex = 0;
        int i = 1;
        while (i < arr.length) {
            for (int j = subEIndex; j >= subSIndex; j--) {
                if (arr[i] == arr[j]) {
                    //切换子串角标前，先和上个子串长度比较
                    int subLength = subEIndex - subSIndex + 1;
                    int lLength = lEIndex - lSIndex + 1;
                    if (subLength > lLength) {
                        lSIndex = subSIndex;
                        lEIndex = subEIndex;
                    }
                    subSIndex = j + 1;
                    subEIndex = i;
                    break;
                } else if (j == subSIndex) {
                    //已经遍历到末尾，尾角标更改
                    subEIndex = i;
                }
            }
            i++;
        }

        int subLength = subEIndex - subSIndex + 1;
        int lLength = lEIndex - lSIndex + 1;
        System.out.println("sub:" + subSIndex + "-" + subEIndex);
        System.out.println("longest:" + lSIndex + "-" + lEIndex);
        return subLength > lLength ? subLength : lLength;
    }


    /**
     * 杨辉三角
     */
    public List<List<Integer>> generate(int numRows) {
        ArrayList<List<Integer>> lists = new ArrayList<List<Integer>>();
        for (int i = 0; i < numRows; i++) {
            ArrayList<Integer> list = new ArrayList<Integer>();
            lists.add(list);
            for (int j = 0; j <= i; j++) {
                if (i == 0 || j == 0 || j == i) {
                    list.add(1);
                } else {
                    ArrayList<Integer> temp = (ArrayList<Integer>) lists.get(i - 1);
                    Integer value = temp.get(j) + temp.get(j - 1);
                    list.add(value);
                }
            }
        }
        return lists;
    }

    /**
     * 获取杨辉三角第n行
     */
    public List<Integer> getRow(int n) {
        List<Integer> res = new ArrayList<>(n + 1);
        long cur = 1;
        for (int i = 0; i <= n; i++) {
            res.add((int) cur);
            cur = cur * (n - i) / (i + 1);
        }
        return res;
    }

    /**
     * 判断是否回文串
     */
    public boolean isPalindrome(String s) {
        if (s == null) return false;
        if (s.length() == 0) return true;
        int sIndex = 0;
        int endIndex = s.length() - 1;
        while (sIndex < endIndex) {
            int sChr = s.charAt(sIndex);
            if (!isDigitOrChar(sChr)) {
                sIndex++;
                continue;
            }
            int eChr = s.charAt(endIndex);
            if (!isDigitOrChar(eChr)) {
                endIndex--;
                continue;
            }

            sChr = lowcase(sChr);
            eChr = lowcase(eChr);
            if (sChr != eChr) {
                return false;
            }
            sIndex++;
            endIndex--;
        }
        return true;
    }

    int lowcase(int v) {
        if (v >= 97 && v <= 122) {
            return v - 32;
        }
        return v;
    }

    boolean isDigitOrChar(int v) {
        return (v >= 48 && v <= 57) || (v >= 65 && v <= 90) || (v >= 97 && v <= 122);
    }
    /* 判断是否回文串 --end ===============================================================  */


    /**
     * 38 报数
     */
    public String countAndSay(int n) {
        if (n <= 1) return "1";
        StringBuilder result = null;
        String tempStr = "1";
        while (n > 1) {
            n--;
            result = new StringBuilder();
            int count = 0;
            char tempChar = 0;
            for (int j = 0; j < tempStr.length(); j++) {
                char cChar = tempStr.charAt(j);
                if (tempChar == 0) {
                    tempChar = cChar;
                    count++;
                } else if (tempChar == cChar) {
                    count++;
                } else {
                    result.append(count).append(tempChar);
                    tempChar = cChar;
                    count = 1;
                }

                if (j == tempStr.length() - 1) {
                    result.append(count).append(tempChar);
                }
            }
            tempStr = result.toString();
        }
        return result.toString();
    }


    /**
     * 53. 最大子序和
     */
    public int maxSubArray(int[] nums) {
        if (nums == null || nums.length == 0) return 0;
        int max = nums[0];
        int temp = 0;
        for (int num : nums) {
            if (temp > 0) {
                temp += num;
            } else {
                temp = num;
            }
            max = max > temp ? max : temp;
        }
        return max;
    }


    /**
     * 14.最长公共前缀
     */
    public String longestCommonPrefix(String[] strs) {
        if (strs == null || strs.length == 0) return "";
        StringBuilder sb = new StringBuilder();
        for (String str : strs) {
            if (str == null || str.equals("")) return "";
            if (sb.length() == 0) {
                sb.append(str);
                continue;
            } else if (sb.length() > str.length()) {
                sb.replace(str.length(), sb.length(), "");
            }

            for (int i = 0; i < sb.length(); i++) {
                if (i >= str.length()) break;

                if (str.charAt(i) != sb.charAt(i)) {
                    if (i == 0) return "";
                    sb.replace(i, sb.length(), "");
                    break;
                }
            }
        }
        return sb.toString();
    }

    /**
     * 竞赛题 判断三点是否在同一直线
     *
     * @param points
     * @return
     */
    public boolean isBoomerang(int[][] points) {
        return checkK(points[0][0], points[0][1], points[1][0], points[1][1], points[2][0], points[2][1]);
    }

    /**
     * 竞赛题：判断三点是否同一直线internal
     *
     * @return
     */
    boolean checkK(int x1, int y1, int x2, int y2, int x3, int y3) {
        int retsult = x1 * y2 - x2 * y1 + x2 * y3 - x3 * y2 + x3 * y1 - x1 * y3;
        return retsult > 0;
    }

    /**
     * 36.有效的数独
     * 此处由于board的长度有限，所以计算hash的耗时会比遍历数组本身更多一些
     */
    public boolean isValidSudoku(char[][] board) {
        Map<String, String> map = new HashMap<>(81);
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (board[i][j] != '.') {
                    String rowkv = "r" + i + board[i][j];
                    String clokv = "c" + j + board[i][j];
                    String square = "sq" + (i / 3) + (j / 3) + board[i][j];
                    String rv = map.get(rowkv);
                    String cv = map.get(clokv);
                    String sv = map.get(square);
                    if (rv != null && rv.equals(rowkv)) return false;
                    if (cv != null && cv.equals(clokv)) return false;
                    if (sv != null && sv.equals(square)) return false;

                    map.put(rowkv, rowkv);
                    map.put(clokv, clokv);
                    map.put(square, square);
                }
            }
        }
        return true;
    }

    /**
     * 37.解数独
     */
    public void solveSudoku(char[][] board) {

        for (char[] cs : board) {
            System.out.print(Arrays.toString(cs));
        }
    }


    /**
     * 8.字符串转换整数
     * 根据题设，
     * 1.空白符当成忽略字符
     * 2.第一个出现的非空白字符必须是数字或者是"-"其余符号导致无效
     * 3.数据有效性为int值上下限
     */
    public int myAtoi(String str) {
        int flag = 2;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (flag == 2) {
                if (' ' == c) continue;
                if ('-' == c) {
                    flag = -1;
                } else if ('0' <= c && c <= '9') {
                    if ('0' != c)
                        sb.append(c);
                    flag = 1;
                } else if ('+' == c) {
                    flag = 1;
                } else {
                    return 0;
                }
            } else if ('0' <= c && c <= '9') {
                if (sb.length() == 0 && '0' == c) continue;
                sb.append(c);
            } else {
                break;
            }
        }

        if (sb.length() > 10)
            return flag == 1 ? 2147483647 : -2147483648;

        long result = 0;
        for (int j = 0; j < sb.length(); j++) {
            result *= 10;
            result += sb.charAt(j) - '0';
        }
        result *= flag;

        if (result > 2147483647) return 2147483647;
        else if (result < -2147483648) return -2147483648;
        return (int) result;
    }


    /**
     * 5.最长回文子串
     * 方法一:暴力破解
     */
    public String longestPalindrome1(String s) {
        if (null == s || "".equals(s)) return "";
        char[] arr = s.toCharArray();
        if (checkPalindrome(arr, 0, arr.length - 1)) return s;
        int len = arr.length;
        int left = 0;
        int subLen = s.length() - 1;
        while (subLen > 0) {
            int right = subLen;
            while (right <= len) {
//                此处使用substring会严重影响内存使用
//                String sub = s.substring(left,right);
                if (checkPalindrome(arr, left, right - 1)) {
                    return s.substring(left, right);
                }
                left++;
                right++;
            }
            subLen--;
            left = 0;
        }
        return "";
    }

    private boolean checkPalindrome(char[] s, int left, int right) {
        while (left <= right) {
            if (s[left] == s[right]) {
                left++;
                right--;
            } else {
                return false;
            }
        }
        return true;
    }

    /**
     * 5.最长回文串
     * 中心拓展法
     */
    private int subl, subr;

    public String longestPalindrome(String s) {
        for (int i = 0; i < s.length(); i++) {
            expand(s, i, i);
            expand(s, i, i + 1);
        }
        return s.substring(subl, subl + subr);
    }

    //5.最长回文 私有
    private void expand(String s, int left, int right) {
        while (left >= 0 && right <= s.length() - 1 && s.charAt(left) == s.charAt(right)) {
            left--;
            right++;
        }
        if (subr < right - left - 1) {
            subr = right - left - 1;
            subl = left + 1;
        }
    }


    /**
     * 29.两数相除
     */
    public int divide(int dividend, int divisor) {
        if (dividend == 1 << 31 && divisor == -1) return (1 << 31) - 1;
        int a = Math.abs(dividend), b = Math.abs(divisor), res = 0, x = 0;
        while (a - b >= 0) {
            for (x = 0; a - (b << x << 1) >= 0; x++) ;
            res += 1 << x;
            a -= b << x;
        }
        return (dividend > 0) == (divisor > 0) ? res : -res;
    }

    /**
     * 竞赛题5063
     * 最后一块石头的重量
     */
    public int lastStoneWeight(int[] stones) {
        int result = stones[0];
        while (stones.length >= 2) {
            sort(stones);
            if (stones[0] == 0)
                break;
            if (stones[1] == 0) {
                return stones[0];
            }
            result = stones[0] - stones[1];
            stones[0] = result;
            stones[1] = 0;
        }
        return result;
    }

    /**
     * * 竞赛题5063
     * 最后一块石头的重量
     */
    private void sort(int[] stones) {
        boolean flag = false;
        for (int i = 0; i < stones.length - 1; i++) {
            for (int j = stones.length - 1; j > i; j--) {
                if (stones[j] > stones[j - 1]) {
                    int temp = stones[j];
                    stones[j] = stones[j - 1];
                    stones[j - 1] = temp;
                    flag = true;
                }
            }
            if (!flag) break;
        }
    }

    /**
     * 竞赛题5063
     * 最后一块石头的重量
     */
    public int lastStoneWeight1(int[] stones) {
        int result = 0;
        int large1 = 0, large2 = 0;
        int tempindex1 = 0, tempindex2 = 0;
        int i = 0;
        while (i <= stones.length) {
            int temp = stones[i];
            if (temp > large1) {
                large1 = temp;
                tempindex1 = i;
                continue;
            } else if (temp > large2) {
                large2 = temp;
                tempindex2 = i;
                continue;
            }
            if (i == stones.length) {
                result = large1 - large2;
                i = 0;
            }
            i++;
        }
        return result;
    }


    /**
     * 11.盛最多水的容器
     * 说明:数组长度至少为2
     * 暴力比较，时间复杂度: O(n²)
     */
    public int maxArea1(int[] height) {
        int max = 0;
        int width = height.length - 1;
        int index = 0;
        int tempr = 0;
        int h = 0;
        while (width > 0) {
            index = 0;
            while ((index + width) < height.length) {
                h = height[index] < height[index + width] ? height[index] : height[index + width];
                tempr = h * width;
                max = max > tempr ? max : tempr;
                index++;
            }
            width--;
        }
        return max;
    }

    /**
     * 11.盛最多水的容器
     * 说明:数组长度至少为2
     * 解法2：考虑到面积的大小受长宽的影响，再宽度一定的前提下，长度最矮的一端为容器的盛水上限
     * 所以，从数组的两端开始排查，较短的一端往较长的一端靠拢，依次遍历数组，寻找最大值
     */
    public int maxArea(int[] height) {
        int max = 0;
        int left = 0, right = height.length - 1;
        int temp = 0;
        int h = 0;
        while (left < right) {
            h = height[left] > height[right] ? height[right] : height[left];
            temp = h * (right - left);
            max = max > temp ? max : temp;
            if (height[left] < height[right]) {
                left++;
            } else {
                right--;
            }
        }
        return max;
    }


    /**
     * 12.整数转罗马数字
     * 说明:给定一个整数，将其转为罗马数字。输入确保在 1 到 3999 的范围内
     */
    public String intToRoman1(int num) {
        char[] romans = new char[]{'I', 'V', 'X', 'L', 'C', 'D', 'M'};
        StringBuilder sb = new StringBuilder();
        int temp = num / 1000;
        toRoman(sb, romans, temp, 6);

        temp = num % 1000 / 100;
        toRoman(sb, romans, temp, 4);

        temp = num % 100 / 10;
        toRoman(sb, romans, temp, 2);

        temp = num % 10;
        toRoman(sb, romans, temp, 0);
        return sb.toString();
    }

    /**
     * 12.整数转罗马数字
     * 说明:给定一个整数，将其转为罗马数字。输入确保在 1 到 3999 的范围内
     */
    private void toRoman(StringBuilder sb, char[] romans, int num, int index) {
        for (int i = 0; i < num; i++) {
            if (num == 4) {
                sb.append(romans[index]);
                sb.append(romans[index + 1]);
                break;
            } else if (num == 9) {
                sb.append(romans[index]);
                sb.append(romans[index + 2]);
                break;
            } else if (num >= 5) {
                sb.append(romans[index + 1]);
                num -= 5;
                i = -1;
            } else {
                sb.append(romans[index]);
            }
        }
    }


    /**
     * 12.整数转罗马数字
     * 说明:给定一个整数，将其转为罗马数字。输入确保在 1 到 3999 的范围内
     * 解法二:直接列出特定组合的罗马字符组合
     */
    public String intToRoman(int num) {
        StringBuilder sb = new StringBuilder();
        int[] intvalue = {1, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500, 900, 1000};
        String[] romans = {"I", "IV", "V", "IX", "X", "XL", "L", "XC", "C", "CD", "D", "CM", "M"};
        for (int i = intvalue.length - 1; i >= 0; i--) {
            if (num >= intvalue[i]) {
                int count = num / intvalue[i];
                while (count > 0) {
                    count--;
                    sb.append(romans[i]);
                }
                num %= intvalue[i];
            }
        }
        return sb.toString();
    }


    /**
     * 16.最接近的三数之和
     * 解法:直接暴力求解时间复杂度为O(n³），另一种思路是，先把数组排序，再使用首位指针逐个比较
     */
    public int threeSumClosest(int[] nums, int target) {
        long sum = Integer.MAX_VALUE;
        int temp;
        Arrays.sort(nums);

        for (int i = 0; i < nums.length; i++) {
            int left = i + 1;
            int right = nums.length - 1;
            while (left < right) {
                temp = nums[i] + nums[left] + nums[right];
                if (temp == target) return target;

                sum = Math.abs(temp - target) < Math.abs(sum - target) ? temp : sum;
                if (temp - target > 0) {
                    right--;
                } else {
                    left++;
                }
            }
        }
        return (int) sum;
    }


    /**
     * 6.Z字形变换
     */
    public String zConvert(String s, int numRows) {
        if (numRows == 1) return s;

        StringBuilder ret = new StringBuilder();
        int n = s.length();
        int cycleLen = 2 * numRows - 2;

        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j + i < n; j += cycleLen) {
                ret.append(s.charAt(j + i));
                if (i != 0 && i != numRows - 1 && j + cycleLen - i < n)
                    ret.append(s.charAt(j + cycleLen - i));
            }
        }
        return ret.toString();
    }


    /**
     * 17. 电话号码的字母组合
     * 给定一个仅包含数字 2-9 的字符串，返回所有它能表示的字母组合。
     * 采用队列法
     */
    public List<String> letterCombinations(String digits){
        String[] letters = new String[]{"abc","def","ghi","jkl","mno","pqrs","tuv","wxyz"};
        char[] nums = digits.toCharArray();
        List<String> list = new ArrayList<>();
        if(nums.length>0)
            list.add("");
        for(char c:nums){
            String value = letters[c-'2'];
            int size = list.size();
            for(int i=0;i<size;i++){
                String tempStr = list.remove(0);
                for(int j=0;j<value.length();j++){
                    list.add(tempStr+value.charAt(j));
                }
            }
        }
        return list;
    }


    /**
     * 18.四数之和
     * 给你一个由 n 个整数组成的数组 nums ，和一个目标值 target 。请你找出并返回满足下述全部条件且不重复的四元组 [nums[a], nums[b], nums[c], nums[d]] ：
     *
     * 0 <= a, b, c, d < n
     * a、b、c 和 d 互不相同
     * nums[a] + nums[b] + nums[c] + nums[d] == target
     * 你可以按 任意顺序 返回答案
     *
     * 来源：力扣（LeetCode）
     * 链接：https://leetcode-cn.com/problems/4sum
     * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
     */
//    public List<List<Integer>> fourSum(int[] nums, int target) {
//        List<List<Integer>> allS = new ArrayList<>();
//
//        for(int i=0;i<nums.length;i++){
//
//        }
//
//        return allS;
//    }
//
//    private List<Integer> fourSum_18(int index,int[] nums,int target){
//        int times = 3;
//        while(index<=nums.length-1){
//            target -= nums[index];
//        }
//    }
}
