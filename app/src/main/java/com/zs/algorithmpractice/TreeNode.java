package com.zs.algorithmpractice;

/**
 * @author: zhangshao
 * @time: 2019/5/31 10:41
 * @description:
 */
public class TreeNode {
    public int val;
    public TreeNode left;
    public TreeNode right;
    public TreeNode(int x) {
        val = x;
    }
}
